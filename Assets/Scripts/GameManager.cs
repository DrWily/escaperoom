﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public bool hasRed, hasGreen, hasBlue;
    private bool isKonamiOn, isAtIntro;
    public int gemCount;
    public int actualMission;
    public int activateSourceMission, placeStoneMission, encryptedMission, konamiMission, konamiMission2, finalMission;
    private int konamiSteps;
    private KeyCode[] konamiKodeInputs = new KeyCode[] {KeyCode.W, KeyCode.W, KeyCode.S, KeyCode.S,
    KeyCode.A, KeyCode.D, KeyCode.A, KeyCode.D};
    private KeyCode[] konamiKodeInputsAlt = new KeyCode[] {KeyCode.UpArrow, KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.DownArrow,
    KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.LeftArrow, KeyCode.RightArrow};
    [SerializeField] private GameObject stoneDropper, exitPrefab, formMenu, endMenu, panelBG, playerPrefab;
    [SerializeField] private SourceBehavior source;
    [SerializeField] private List<AudioSource> musics;
    string[] konamiDialog = new string[] { "Se sabes esse código, com certeza és um sobrevivente de tempos imemoriais." };

    void Awake()
    {
        //Singleton
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        hasRed = false;
        hasBlue = false;
        hasGreen = false;
        isKonamiOn = false;
        isAtIntro = true;
        konamiSteps = 0;
        gemCount = 0;
        actualMission = 0;
        exitPrefab.SetActive(false);
        playerPrefab.SetActive(false);

    }

    // Use this for initialization
    void Start () {
        isAtIntro = true;
        AudioManager.instance.Play("IntroTheme");
        LoadForm();
	}
	
	// Update is called once per frame
	void Update () {
        if (isKonamiOn)
            KonamiKode();
	}

    void ActivateSource()
    {
        source.Activate();
    }

    void PlaceStone()
    {
        source.PlaceStone();
    }

    public void NextMission()
    {
        if (actualMission == activateSourceMission)
            ActivateSource();
        if (actualMission == placeStoneMission)
            PlaceStone();
        if (actualMission == konamiMission)
            StartReadingKode();

        actualMission++;
    }

    /*public void UpdateNES()
    {
        GameObject nes2 = Instantiate(nesObject2, nesObject1.transform.position, nesObject1.transform.rotation);
        Destroy(nesObject1);
    }*/

    public void DropStone()
    {
        stoneDropper.GetComponent<StoneDropper>().DropStone();
    }

    public void StartReadingKode()
    {
        isKonamiOn = true;
    }

    public void StopReadingKode()
    {
        isKonamiOn = false;
    }

    public void KonamiKode()
    {
        if (Input.anyKeyDown)
        {
            if (Input.GetKeyDown(konamiKodeInputs[konamiSteps]) || Input.GetKeyDown(konamiKodeInputsAlt[konamiSteps]))
            {
                konamiSteps++;
                Debug.Log("Got one right");
                if (konamiSteps == konamiKodeInputs.Length)
                {
                    DialogueManager.instance.ShowDialog(konamiDialog, konamiMission2);
                    Debug.Log("All right");
                    StopReadingKode();
                    DropStone();
                }
            }
            else
            {
                konamiSteps = 0;
                Debug.Log("Got one wrong");
            }
        }
    }

    public void CreateExit()
    {
        exitPrefab.SetActive(true);
    }

    public void FinishGame()
    {
        if(actualMission == finalMission)
        {
            AudioManager.instance.Play("EndTheme");
            Debug.Log("Finished!");
            playerPrefab.SetActive(false);
            LoadForm();
        }
    }

    public void LoadForm()
    {
        //Open a GUI here
        panelBG.SetActive(true);
        if (actualMission == 0)
            formMenu.SetActive(true);
        else
            endMenu.SetActive(true);

    }

    public void StartGame()
    {
        AudioManager.instance.Play("MainTheme");
        playerPrefab.SetActive(true);
    }

    public void LoadIntro()
    {
        panelBG.SetActive(false);
        formMenu.SetActive(false);
        DialogueManager.instance.ShowIntroDialog();
    }

    public void AddStone(string name)
    {
        if (name == "Vermelho")
            hasRed = true;
        else if (name == "Verde")
            hasGreen = true;
        else if(name == "Azul")
        {
            hasBlue = true;
            stoneDropper.GetComponent<StoneDropper>().DestroySapphire();
        }
        instance.gemCount++;
    }
}
