﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Form : MonoBehaviour {

    public void GoToForm()
    {
        //TODO add correct link
        Debug.Log("Open Form in Browser");
        Application.OpenURL("https://docs.google.com/forms/d/e/1FAIpQLScnGKErh1CV9Wk68A5rWf1rlhawkrJJqXK2bFSQv4ZQQUh9gg/viewform?usp=sf_link");
    }
    public void Continue()
    {
        Debug.Log("Begin");
        GameManager gm = GameManager.instance;
        gm.LoadIntro();
    }
    public void Quit()
    {
        Application.Quit();
    }
}
