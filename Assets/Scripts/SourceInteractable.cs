﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SourceInteractable : InteractableBHV {

    protected override void MessageHandler(string[] speech)
    {
        if (GetComponent<SourceBehavior>().HasStone())
            DialogueManager.instance.ShowDialogWithButton(speech);
        else
        {
            string[] placeStoneText = new string[1];
            placeStoneText[0] = "Você inseriu a pedra mágica VERDE...";
            DialogueManager.instance.ShowDialog(placeStoneText, missionOrder);
        }
    }
}
