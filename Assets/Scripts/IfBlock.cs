﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IfBlock : MonoBehaviour {

    [SerializeField]
    private string ifColor;
    [SerializeField]
    private float launchSpeed;

    private int stayCount;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = 0;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = stayCount + 1;
            if (stayCount == 10)
            {
                string actualColor = ((collision.gameObject).GetComponent<LightBHV>()).color;
                if(actualColor == ifColor)
                {
                    Rigidbody2D body = collision.gameObject.GetComponent<Rigidbody2D>();
                    body.velocity = new Vector2(0, 0);
                    body.AddForce(new Vector2(launchSpeed, 0), ForceMode2D.Impulse);
                }
            }
        }
    }
}
