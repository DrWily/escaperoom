﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour {

    public float moveSpeed = 1f;
    Animator thisAnim;
    float lastX, lastY;

    [SerializeField]
    private Vector2 _deltaForce;
    private Rigidbody2D _rBody;
    private BoxCollider2D _bCollider;

    void Awake()
    {
        _rBody = GetComponent<Rigidbody2D>();
        _bCollider = GetComponent<BoxCollider2D>();
        thisAnim = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start () {
        _rBody.gravityScale = 0;
        _rBody.constraints = RigidbodyConstraints2D.FreezeRotation;
	}
	
	// Update is called once per frame
	void Update () {
        CheckInput();
	}

    void CheckInput()
    {
        var _H = Input.GetAxisRaw("Horizontal");
        var _V = Input.GetAxisRaw("Vertical");

        _deltaForce = new Vector2(_H, _V);
        CalculateMovement(_deltaForce * moveSpeed);
    }

    void CalculateMovement(Vector2 _playerForce)
    {
        UpdateAnimation(_playerForce);
        _rBody.velocity = Vector2.zero;
        _rBody.AddForce(_playerForce, ForceMode2D.Impulse);
    }

    void UpdateAnimation(Vector2 dir)
    {
        dir.Normalize();
        if(dir.x == 0f && dir.y == 0f)
        {
            thisAnim.SetFloat("LastDirX", lastX);
            thisAnim.SetFloat("LastDirY", lastY);
            thisAnim.SetBool("Movement", false);
        }
        else
        {
            lastX = dir.x;
            lastY = dir.y;
            thisAnim.SetBool("Movement", true);
        }
        thisAnim.SetFloat("DirX", dir.x);
        thisAnim.SetFloat("DirY", dir.y);
    }
}
