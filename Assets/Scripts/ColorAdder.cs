﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorAdder : MonoBehaviour {

    [SerializeField]
    private Color newColor;

    private int stayCount;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = 0;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = stayCount + 1;
            if (stayCount == 20)
            {
                Color actualColor = ((collision.gameObject).GetComponent<SpriteRenderer>()).color;
                actualColor = new Color(Mathf.Clamp(actualColor.r+newColor.r, 0.0f, 1.0f), Mathf.Clamp(actualColor.g + newColor.g, 0.0f, 1.0f), Mathf.Clamp(actualColor.b + newColor.b, 0.0f, 1.0f));
                ((collision.gameObject).GetComponent<SpriteRenderer>()).color = actualColor;
                //sr.color = newColor;
            }
        }
    }
}
