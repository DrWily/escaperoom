﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizInteractable : InteractableBHV
{
    [SerializeField]
    protected string answer;
    protected override void MessageHandler(string[] speech)
    {
        DialogueManager.instance.ShowDialogQuiz(speech, answer, missionOrder);
    }
}