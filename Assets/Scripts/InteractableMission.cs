﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableMission : InteractableBHV {

    protected override void MessageHandler(string[] speech)
    {
        DialogueManager.instance.ShowDialog(speech, missionOrder);
    }
}
