﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SapphireBHV : MonoBehaviour {

    private bool isSlowing;
    [SerializeField] private float speedReduction, minSpeed;
    private int counter;
    private Vector2 speed;

    private void Awake()
    {
        isSlowing = false;
        counter = 0;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(isSlowing)
        {
            if (counter < 10)
                counter++;
            else
            {
                speed = GetComponent<Rigidbody2D>().velocity;
                if (speed.y < -minSpeed)
                {
                    speed.y = speed.y + speedReduction;
                    GetComponent<Rigidbody2D>().velocity = speed;
                }
                counter = 0;
            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "StopPoint")
        {
            isSlowing = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isSlowing = false;
        if (collision.gameObject.name == "StopPoint")
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
}
