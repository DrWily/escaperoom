﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveBHV : MonoBehaviour
{

    [SerializeField]
    private string color;
    [SerializeField]
    private Sprite activeSprite;
    private int stayCount;
    private bool active;

    void Awake()
    {
        active = false;
        stayCount = 0;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = 0;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = stayCount + 1;
            if (stayCount == 10)
            {
                if (!active)
                {
                    string otherColor = (collision.gameObject).GetComponent<LightBHV>().color;
                    if (color == otherColor)
                    {
                        GetComponent<SpriteRenderer>().sprite = activeSprite;
                        Debug.Log("Activate receiver");
                        active = true;
                        GameManager.instance.NextMission();
                        if(GameManager.instance.actualMission == GameManager.instance.finalMission)
                        {
                            GameManager.instance.CreateExit();
                        }
                    }
                }
                Rigidbody2D body = collision.gameObject.GetComponent<Rigidbody2D>();
                Destroy(body.gameObject);
            }
        }
    }
}
