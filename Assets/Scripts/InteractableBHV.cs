﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableBHV : MonoBehaviour {

    protected bool isInTrigger;
    [SerializeField]
    [TextArea(4, 6)]
    protected string[] speech;

    [SerializeField]
    protected int missionOrder;

    protected void Awake()
    {
        isInTrigger = false;
    }
    
	
	// Update is called once per frame
	protected void Update () {
        if (isInTrigger && Input.GetKeyDown(KeyCode.Space) && !DialogueManager.instance.dialogActive)
        {
            Debug.Log("Chat");
            MessageHandler(speech);
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("EnterPlayer");
            Debug.Log(DialogueManager.instance.dialogActive);
            isInTrigger = true;
        }
    }

    protected void OnTriggerExit2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("ExitPlayer");
            isInTrigger = false;
        }
    }


    protected abstract void MessageHandler(string[] speech);
}