﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour {

    [SerializeField]
    private Color newColor;

    private int stayCount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Energy")
        {
            stayCount = 0;
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Energy")
        {
            stayCount = stayCount + 1;
            if (stayCount == 20)
            {
                ((collision.gameObject).GetComponent<SpriteRenderer>()).color = new Color(newColor.r, newColor.g, newColor.b);
                //sr.color = newColor;
            }
        }
    }
}
