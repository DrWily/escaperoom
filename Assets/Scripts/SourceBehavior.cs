﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SourceBehavior : MonoBehaviour {

    [SerializeField]
    private float countdown = 3.0f;
    private float launchSpeed = 5.0f;
    [SerializeField]
    private Sprite activeSprite;
    private bool isActive;
    private bool hasStone;
    private string actualLight;

    public Rigidbody2D energyPrefab;

    void Awake()
    {
        isActive = false;
        hasStone = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(hasStone)
        {
            countdown -= Time.deltaTime;
            if (countdown < 0.0f)
            {
                SpawnLight();
                countdown = 3.0f;
            }
        }
    }

    void SpawnLight()
    {
        Rigidbody2D energy = Instantiate(energyPrefab, transform.position, transform.rotation);
        energy.velocity = Vector2.zero;
        energy.GetComponent<LightBHV>().SetColor(actualLight);
        energy.AddForce(new Vector2(0, launchSpeed), ForceMode2D.Impulse);
    }

    public void Activate()
    {
        GetComponent<SpriteRenderer>().sprite = activeSprite;
    }

    public bool HasStone()
    {
        return hasStone;
    }

    public void PlaceStone()
    {
        hasStone = true;
        actualLight = "Verde";
    }

    public void ChangeStone(string color)
    {
        actualLight = color;
    }
}
