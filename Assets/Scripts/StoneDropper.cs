﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneDropper : MonoBehaviour {

    [SerializeField] private GameObject stone;
    [SerializeField] private float dropSpeed;
    public GameObject sapphire;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DropStone()
    {
        Debug.Log("Dropping Stone");
        sapphire = Instantiate(stone, transform.position, transform.rotation);
        sapphire.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -dropSpeed);
    }

    public void DestroySapphire()
    {
        Destroy(sapphire);
    }
}
