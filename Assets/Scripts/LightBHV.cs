﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBHV : MonoBehaviour {

    public string color;
    public Sprite[] actualSprite;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetColor(string newColor)
    {
        color = newColor;
        if (newColor == "Vermelho")
            GetComponent<SpriteRenderer>().sprite = actualSprite[0];
        else if (newColor == "Azul")
            GetComponent<SpriteRenderer>().sprite = actualSprite[1];
        else if (newColor == "Verde")
            GetComponent<SpriteRenderer>().sprite = actualSprite[2];
        else
            Debug.Log("Wrong Color Name");
        GetComponent<SpriteRenderer>().sortingOrder = 3;

    }
}
