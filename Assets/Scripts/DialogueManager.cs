﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public static DialogueManager instance = null;
    public string[] dialogLines;
    public Button[] colorButtons;
    private Button[] activeButtons;
    public int buttonSelected;
    public Font encriptedFont;
    
    
    [SerializeField]
    private GameObject dBox, introBox;
    [SerializeField]
    private GameObject lightSource;
    [SerializeField] [TextArea(4, 6)] private string[] introQuotes;
    public Text dText, introText;
    public bool dialogActive, hasButton, isQuiz, isKonami, isKonamiAgain;
    private int currentLine, nActiveButtons;
    private bool isEncrypted, isIntro;

    private string[] notYet, alreadyDone;
    private string quizAnswer;


    void Awake()
    {
        //Singleton
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        dialogActive = false;
        currentLine = 0;
        buttonSelected = 0;
        hasButton = false;
        isQuiz = false;
        isIntro = false;
        activeButtons = null;

        notYet = new string[1];
        notYet[0] = "Agora nao é hora para isso";

        alreadyDone = new string[1];
        alreadyDone[0] = "Nada mais para fazer aqui";

        isEncrypted = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (dialogActive)
        {
            if (hasButton)
            {
                activeButtons[buttonSelected].GetComponent<Outline>().enabled = true;
                if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow)))
                {
                    if (buttonSelected > 0)
                    {
                        activeButtons[buttonSelected].GetComponent<Outline>().enabled = false;
                        buttonSelected--;
                        activeButtons[buttonSelected].GetComponent<Outline>().enabled = true;
                    }
                }
                else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                {
                    if (buttonSelected < (nActiveButtons-1))
                    {
                        activeButtons[buttonSelected].GetComponent<Outline>().enabled = false;
                        buttonSelected++;
                        activeButtons[buttonSelected].GetComponent<Outline>().enabled = true;
                    }
                }
                else if (Input.GetKeyDown(KeyCode.X))
                {
                    foreach (Button b in colorButtons)
                    {
                        b.gameObject.SetActive(false);
                    }
                    if (isQuiz)
                    {
                        if (colorButtons[buttonSelected].GetComponentInChildren<Text>().text == quizAnswer)
                        {
                            dText.text = "Correto! Você obteve o cristal " + activeButtons[buttonSelected].GetComponentInChildren<Text>().text;
                            GameManager.instance.NextMission();
                            GameManager.instance.AddStone(quizAnswer);
                        }
                        else
                        {
                            dText.text = "Nada aconteceu... Deve ser outra resposta.";
                        }
                    }
                    else
                    {
                        dText.text = "Você selecionou o cristal " + activeButtons[buttonSelected].GetComponentInChildren<Text>().text;
                        lightSource.GetComponent<SourceBehavior>().ChangeStone(activeButtons[buttonSelected].GetComponentInChildren<Text>().text);
                    }
                    activeButtons[buttonSelected].GetComponent<Outline>().enabled = false;
                    hasButton = false;
                    isQuiz = false;
                    activeButtons = null;
                }
            }

            else if (Input.GetKeyDown(KeyCode.X))
            {
                currentLine++;
                if (isEncrypted)
                {
                    dText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
                    isEncrypted = false;
                }
                if (currentLine >= dialogLines.Length)
                {
                    
                    if (isIntro)
                    {
                        introBox.SetActive(false);
                        isIntro = false;
                        dialogActive = false;
                        currentLine = 0;
                        GameManager.instance.StartGame();
                    }
                    else
                        dBox.SetActive(false);

                    dialogActive = false;
                    currentLine = 0;
                }
                if (isIntro)
                    introText.text = dialogLines[currentLine];
                else
                    dText.text = dialogLines[currentLine];
            }
        }
    }

    public void ShowBox(string dialog)
    {
        dialogActive = true;
        dBox.SetActive(true);
        dText.text = dialog;
    }

    public void ShowDialogWithButton(string[] dialog)
    {
        hasButton = true;
        isQuiz = false;
        dialogLines = dialog;
        currentLine = 0;
        buttonSelected = 0;
        dText.text = dialogLines[currentLine];
        

        nActiveButtons = 0;
        if (GameManager.instance.gemCount > 0)
        {
            activeButtons = new Button[3];
            Debug.Log("Let's Search Buttons");
            for(int i = 0; i < colorButtons.Length; ++i)
            {
                if (GameManager.instance.hasBlue)
                {
                    if (colorButtons[i].GetComponentInChildren<Text>().text == "Azul")
                    {
                        Debug.Log("Blue Buttom");
                        colorButtons[i].gameObject.SetActive(true);
                        activeButtons[nActiveButtons++] = colorButtons[i];
                    }
                }
                if (GameManager.instance.hasRed)
                {
                    if (colorButtons[i].GetComponentInChildren<Text>().text == "Vermelho")
                    {
                        Debug.Log("Red Buttom");
                        colorButtons[i].gameObject.SetActive(true);
                        activeButtons[nActiveButtons++] = colorButtons[i];
                    }
                }
                if (GameManager.instance.hasGreen)
                {
                    if (colorButtons[i].GetComponentInChildren<Text>().text == "Verde")
                    {
                        Debug.Log("Green Buttom");
                        colorButtons[i].gameObject.SetActive(true);
                        activeButtons[nActiveButtons++] = colorButtons[i];
                    }
                }
            }
        }
        else
        {
            dText.text = "Você não pode fazer nada aqui :(";
            hasButton = false;
        }
        dialogActive = true;
        dBox.SetActive(true);
    }

    public void ShowDialog(string[] dialog, int questIndex)
    {
        hasButton = false;
        isQuiz = false;
        isKonami = false;
        isKonamiAgain = false;
        if (questIndex == GameManager.instance.konamiMission2)
            GameManager.instance.actualMission = questIndex;

        if (questIndex < GameManager.instance.actualMission)
            dialogLines = alreadyDone;
        else if (questIndex > GameManager.instance.actualMission)
            dialogLines = notYet;
        else
        {
            if (questIndex == GameManager.instance.encryptedMission)
            {
                isEncrypted = true;
                dText.font = encriptedFont;
            }
            dialogLines = dialog;
            if((GameManager.instance.konamiMission2-1) != questIndex)
                GameManager.instance.NextMission();
        }
        currentLine = 0;
        dText.text = dialogLines[currentLine];
        dialogActive = true;
        dBox.SetActive(true);
    }

    public void ShowDialogQuiz(string[] dialog, string answer, int questIndex)
    {
        if (questIndex < GameManager.instance.actualMission)
            dialogLines = alreadyDone;
        else if (questIndex > GameManager.instance.actualMission)
            dialogLines = notYet;
        else
        {
            hasButton = true;
            isQuiz = true;
            quizAnswer = answer;
            dialogLines = dialog;
            activeButtons = new Button[3];
            nActiveButtons = 0;
            for (int i = 0; i < colorButtons.Length; ++i)
            {
                colorButtons[i].gameObject.SetActive(true);
                activeButtons[nActiveButtons++] = colorButtons[i];
            }
        }
        currentLine = 0;
        dText.text = dialogLines[currentLine];
        dialogActive = true;
        dBox.SetActive(true);
    }

    public void ShowIntroDialog()
    {
        isIntro = true;
        currentLine = 0;
        dialogLines = introQuotes;
        introText.text = dialogLines[currentLine];
        dialogActive = true;
        introBox.SetActive(true);
    }
}
